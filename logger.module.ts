import { NgModule, ModuleWithProviders } from '@angular/core';
import { LoggerServiceConfig } from './logger.service';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [],
  providers: [BxLoggerModule]
})
export class BxLoggerModule {
  static forRoot(config: LoggerServiceConfig): ModuleWithProviders<BxLoggerModule> {
    return {
      ngModule: BxLoggerModule,
      providers: [
        { provide: LoggerServiceConfig, useValue: config }
      ]
    };
  }
}
