import { Injectable, Optional } from '@angular/core';
import { LoggerLevel } from './logger.levels';

export class LoggerServiceConfig {
  level: LoggerLevel = LoggerLevel.WARN;
}

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor(@Optional() config?: LoggerServiceConfig) {
    let level = LoggerLevel.WARN;

    if (config) {
      level = config.level;
    }

    this.setLogLevel(level);
  }

  public setLogLevelFromString(level: string) {
    const newLevel = LoggerService.LogLevelFromString(level);
    if (newLevel !== undefined) {
      this.setLogLevel(newLevel);
    }
  }

  static LogLevelFromString(level: string) {
    if (!level)
      return undefined;

    const l = level.toLowerCase();

    switch (l) {
      case 'trace':
        return LoggerLevel.TRACE;
      case 'debug':
        return LoggerLevel.DEBUG;
      case 'info':
        return LoggerLevel.INFO;
      case 'log':
        return LoggerLevel.LOG;
      case 'warn':
        return LoggerLevel.WARN;
      case 'error':
        return LoggerLevel.ERROR;
      case 'fatal':
        return LoggerLevel.FATAL;
      case 'off':
        return LoggerLevel.OFF;
    }
    return undefined;
  }

  private no_log = () => { };

  public setLogLevel(level: LoggerLevel) {


    if (level <= LoggerLevel.LOG) {
      this.log = console.log.bind(console);
    } else {
      this.log = this.no_log;
    }

    if (level <= LoggerLevel.DEBUG) {
      this.debug = console.debug.bind(console);
    } else {
      this.debug = this.no_log;
    }

    if (level <= LoggerLevel.WARN) {
      this.warn = console.warn.bind(console);
    } else {
      this.warn = this.no_log;
    }

    if (level <= LoggerLevel.ERROR) {
      this.error = console.error.bind(console);
    } else {
      this.error = this.no_log;
    }
  }


  public log(_message?: any, ..._optionalParams: any[]): void { }
  public debug(_message?: any, ..._optionalParams: any[]): void { }
  public warn(_message?: any, ..._optionalParams: any[]): void { }
  public error(_message?: any, ..._optionalParams: any[]): void { }
}
